﻿
namespace FileManagementTool
{
    partial class FormStartUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.tabRemoveFiles = new System.Windows.Forms.TabPage();
            this.lblTotalFiles = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxIfAddToDb = new System.Windows.Forms.CheckBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.txtFileCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSearchFiles = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.progressBarStatus = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSourceFolder = new System.Windows.Forms.TextBox();
            this.tabMovingFiles = new System.Windows.Forms.TabPage();
            this.buttonCalc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPercantageInfo = new System.Windows.Forms.Label();
            this.checkBoxBackup = new System.Windows.Forms.CheckBox();
            this.MainTabControl.SuspendLayout();
            this.tabRemoveFiles.SuspendLayout();
            this.tabMovingFiles.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.tabRemoveFiles);
            this.MainTabControl.Controls.Add(this.tabMovingFiles);
            this.MainTabControl.Location = new System.Drawing.Point(26, 74);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(714, 408);
            this.MainTabControl.TabIndex = 0;
            // 
            // tabRemoveFiles
            // 
            this.tabRemoveFiles.Controls.Add(this.checkBoxBackup);
            this.tabRemoveFiles.Controls.Add(this.lblPercantageInfo);
            this.tabRemoveFiles.Controls.Add(this.lblTotalFiles);
            this.tabRemoveFiles.Controls.Add(this.label6);
            this.tabRemoveFiles.Controls.Add(this.checkBoxIfAddToDb);
            this.tabRemoveFiles.Controls.Add(this.buttonStop);
            this.tabRemoveFiles.Controls.Add(this.txtFileCount);
            this.tabRemoveFiles.Controls.Add(this.label5);
            this.tabRemoveFiles.Controls.Add(this.txtSearchFiles);
            this.tabRemoveFiles.Controls.Add(this.label4);
            this.tabRemoveFiles.Controls.Add(this.txtStatus);
            this.tabRemoveFiles.Controls.Add(this.label3);
            this.tabRemoveFiles.Controls.Add(this.buttonStart);
            this.tabRemoveFiles.Controls.Add(this.progressBarStatus);
            this.tabRemoveFiles.Controls.Add(this.label2);
            this.tabRemoveFiles.Controls.Add(this.txtSourceFolder);
            this.tabRemoveFiles.Location = new System.Drawing.Point(4, 22);
            this.tabRemoveFiles.Name = "tabRemoveFiles";
            this.tabRemoveFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabRemoveFiles.Size = new System.Drawing.Size(706, 382);
            this.tabRemoveFiles.TabIndex = 0;
            this.tabRemoveFiles.Text = "Remove Files";
            this.tabRemoveFiles.UseVisualStyleBackColor = true;
            // 
            // lblTotalFiles
            // 
            this.lblTotalFiles.AutoSize = true;
            this.lblTotalFiles.Location = new System.Drawing.Point(554, 176);
            this.lblTotalFiles.Name = "lblTotalFiles";
            this.lblTotalFiles.Size = new System.Drawing.Size(67, 13);
            this.lblTotalFiles.TabIndex = 14;
            this.lblTotalFiles.Text = "Total Files: 0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Progress:";
            // 
            // checkBoxIfAddToDb
            // 
            this.checkBoxIfAddToDb.AutoSize = true;
            this.checkBoxIfAddToDb.Location = new System.Drawing.Point(498, 77);
            this.checkBoxIfAddToDb.Name = "checkBoxIfAddToDb";
            this.checkBoxIfAddToDb.Size = new System.Drawing.Size(159, 17);
            this.checkBoxIfAddToDb.TabIndex = 12;
            this.checkBoxIfAddToDb.Text = "Insert Record into Database";
            this.checkBoxIfAddToDb.UseVisualStyleBackColor = true;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(530, 221);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 11;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // txtFileCount
            // 
            this.txtFileCount.Location = new System.Drawing.Point(508, 120);
            this.txtFileCount.Name = "txtFileCount";
            this.txtFileCount.Size = new System.Drawing.Size(179, 20);
            this.txtFileCount.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(505, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "File Count:";
            // 
            // txtSearchFiles
            // 
            this.txtSearchFiles.Location = new System.Drawing.Point(17, 77);
            this.txtSearchFiles.Name = "txtSearchFiles";
            this.txtSearchFiles.Size = new System.Drawing.Size(100, 20);
            this.txtSearchFiles.TabIndex = 8;
            this.txtSearchFiles.Text = "*.xml";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Search Files:";
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(17, 120);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(484, 20);
            this.txtStatus.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Status:";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(612, 221);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // progressBarStatus
            // 
            this.progressBarStatus.Location = new System.Drawing.Point(17, 192);
            this.progressBarStatus.Name = "progressBarStatus";
            this.progressBarStatus.Size = new System.Drawing.Size(670, 23);
            this.progressBarStatus.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Source Folder:";
            // 
            // txtSourceFolder
            // 
            this.txtSourceFolder.Location = new System.Drawing.Point(17, 38);
            this.txtSourceFolder.Name = "txtSourceFolder";
            this.txtSourceFolder.Size = new System.Drawing.Size(670, 20);
            this.txtSourceFolder.TabIndex = 0;
            this.txtSourceFolder.Text = "\\\\ora-fs1.oraero.com\\WebFiles\\XML\\Submission";
            // 
            // tabMovingFiles
            // 
            this.tabMovingFiles.Controls.Add(this.buttonCalc);
            this.tabMovingFiles.Location = new System.Drawing.Point(4, 22);
            this.tabMovingFiles.Name = "tabMovingFiles";
            this.tabMovingFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabMovingFiles.Size = new System.Drawing.Size(706, 382);
            this.tabMovingFiles.TabIndex = 1;
            this.tabMovingFiles.Text = "Moving Files";
            this.tabMovingFiles.UseVisualStyleBackColor = true;
            // 
            // buttonCalc
            // 
            this.buttonCalc.Location = new System.Drawing.Point(38, 35);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(75, 23);
            this.buttonCalc.TabIndex = 0;
            this.buttonCalc.Text = "Calculation";
            this.buttonCalc.UseVisualStyleBackColor = true;
            this.buttonCalc.Click += new System.EventHandler(this.buttonCalc_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Development tool for purging files!";
            // 
            // lblPercantageInfo
            // 
            this.lblPercantageInfo.AutoSize = true;
            this.lblPercantageInfo.BackColor = System.Drawing.Color.DarkGreen;
            this.lblPercantageInfo.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblPercantageInfo.Location = new System.Drawing.Point(329, 198);
            this.lblPercantageInfo.Name = "lblPercantageInfo";
            this.lblPercantageInfo.Size = new System.Drawing.Size(24, 13);
            this.lblPercantageInfo.TabIndex = 15;
            this.lblPercantageInfo.Text = "% 0";
            // 
            // checkBoxBackup
            // 
            this.checkBoxBackup.AutoSize = true;
            this.checkBoxBackup.Checked = true;
            this.checkBoxBackup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBackup.Location = new System.Drawing.Point(308, 77);
            this.checkBoxBackup.Name = "checkBoxBackup";
            this.checkBoxBackup.Size = new System.Drawing.Size(137, 17);
            this.checkBoxBackup.TabIndex = 16;
            this.checkBoxBackup.Text = "Move to Backup Folder";
            this.checkBoxBackup.UseVisualStyleBackColor = true;
            // 
            // FormStartUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 501);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MainTabControl);
            this.Name = "FormStartUp";
            this.Text = "File Management Tool";
            this.MainTabControl.ResumeLayout(false);
            this.tabRemoveFiles.ResumeLayout(false);
            this.tabRemoveFiles.PerformLayout();
            this.tabMovingFiles.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage tabRemoveFiles;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.ProgressBar progressBarStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSourceFolder;
        private System.Windows.Forms.TabPage tabMovingFiles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearchFiles;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFileCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonCalc;
        private System.Windows.Forms.CheckBox checkBoxIfAddToDb;
        private System.Windows.Forms.Label lblTotalFiles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPercantageInfo;
        private System.Windows.Forms.CheckBox checkBoxBackup;
    }
}

