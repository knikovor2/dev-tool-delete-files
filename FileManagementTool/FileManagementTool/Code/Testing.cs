﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementTool.Code
{
    public static class Testing
    {

        public static void AddRecords(string appName)
        {

            //int checkDatabase = GetRecordsFromLocalDB("264A85A2-844F-4B91-A93A-23BFB88F3948");

            Guid myGuid = Guid.NewGuid();

            TraceSource traceSource = new TraceSource(appName);
            Trace.CorrelationManager.ActivityId = myGuid;

            traceSource.TraceEvent(TraceEventType.Start, 256, $"{appName} is starting right now.");

            for (int i = 1; i <= 10; i++)
            {
                traceSource.TraceEvent(TraceEventType.Information, 8, string.Concat(i.ToString(), $":: {appName} is looping to find right numbers."));
            }

            traceSource.TraceEvent(TraceEventType.Stop, 512, $"{appName} is stopping.");

            traceSource.Flush();
            traceSource.Close();
        }

        public static int GetRecordsFromLoggingDB(string guid_id)
        {
            int result = 0;
            string local_db = string.Empty;
            SqlConnection connection = null;

            try
            {
                local_db = ConfigurationManager.ConnectionStrings["log"].ToString();
                connection = new SqlConnection(local_db);
                connection.Open();
                SqlCommand command = new SqlCommand(string.Concat("SELECT COUNT(*) from [dbo].[Exceptions] where ActivityId = '", guid_id, "'"), connection);
                result = (int)command.ExecuteScalar();
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: " + exp.Message);
            }
            finally
            {
                connection.Close();
            }

            return result;
        }
    }
}
