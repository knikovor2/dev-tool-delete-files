﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileManagementTool.Code
{
    public static class SqlDbLayer
    {

        private static readonly string DbConnectionString = ConfigurationManager.ConnectionStrings["log"].ToString();

        public static int InsertNewRecord(FileInfoAttributes singleFile)
        {
            int iRowsAffected = -1;

            SqlConnection connection = null;
            SqlCommand command = null;
            //SqlParameter sqlParam = null;

            try
            {
                connection = new SqlConnection(DbConnectionString);
                connection.Open();

                command = new SqlCommand("spAddNewFileRow", connection);
                command.CommandType = CommandType.StoredProcedure;

                // param = mCmd.Parameters.Add("RETURN_VALUE", SqlDbType.Int);
                // sqlParam.Direction = ParameterDirection.ReturnValue;

                command.Parameters.Add(new SqlParameter("@FILE_NAME", singleFile.FileName));
                command.Parameters.Add(new SqlParameter("@CREATE_DATE", singleFile.CreatedDate));
                command.Parameters.Add(new SqlParameter("@LAST_ACCESS_DATE", singleFile.LastAccessDate));
                command.Parameters.Add(new SqlParameter("@LAST_WRITE_DATE", singleFile.LastWriteDate));
                command.Parameters.Add(new SqlParameter("@FILE_SIZE", singleFile.Size));

                iRowsAffected = command.ExecuteNonQuery();
                //iReturn = (int)mCmd.Parameters["RETURN_VALUE"].Value;

            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: " + exp.Message);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (command != null) command.Dispose();
            }

            return iRowsAffected;
        }
    }
}
