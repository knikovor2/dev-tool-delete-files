﻿using System;


namespace FileManagementTool.Code
{
    public class FileInfoAttributes
    {

        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastWriteDate { get; set; }
        public DateTime LastAccessDate { get; set; }

        public long Size { get; set; }
    }
}
