﻿using FileManagementTool.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FileManagementTool
{

    public delegate void DelegateDisplayFilesProcess(string sFileName, int iCount);
    public delegate void DelegateThreadFinished();

    public partial class FormStartUp : Form
    {
        private const string START_APP_NAME = "FileManagementTool";

        private int TotalCount { get; set; }
        
        private string BackupFolder
        {
            get
            {
                return Path.Combine(txtSourceFolder.Text, $"Backup_{DateTime.Now.Year}_{DateTime.Now.Month}_{DateTime.Now.Day}");
            }
        }


        // Threads
        Thread m_WorkerThread;

        // delegate
        public DelegateDisplayFilesProcess mDelegateDisplayFilesProcess;
        public DelegateThreadFinished mDelegateThreadFinished;

        // Events
        ManualResetEvent m_EventStopThread;
        ManualResetEvent m_EventThreadStopped;

        ManualResetEvent m_EventStop = null;
        ManualResetEvent m_EventStopped = null;

        public FormStartUp()
        {
            InitializeComponent();

            // initialize delegates
            mDelegateDisplayFilesProcess = new DelegateDisplayFilesProcess(this.DisplayFilesProcess);
            mDelegateThreadFinished = new DelegateThreadFinished(this.ThreadFinished);

            // initialize events
            m_EventStopThread = new ManualResetEvent(false);
            m_EventThreadStopped = new ManualResetEvent(false);
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {

            //double timerNum = double.Parse("21600000");
            // XML Submission Files
            // OraSubmission_05_12_2022_15_46_22

            // test reading data from Logging DB
            //int record = Code.Testing.GetRecordsFromLoggingDB("9B1B935B-A1A3-42C3-96CB-A7DE127582B9");

            // add test records
            // Code.Testing.AddRecords(START_APP_NAME);

            // if (record > 0)
            // {
            // do something
            // }

            // get parameters
            string sourceFolder = txtSourceFolder.Text;
            string searchTxt = txtSearchFiles.Text;

            buttonStart.Enabled = false;

            // create backup folder
            //string backupFolder = Path.Combine(sourceFolder, $"Backup_{DateTime.Now.Year}_{DateTime.Now.Month}_{DateTime.Now.Day}");

            // test minus current date
            //DateTime createdDate = DateTime.Parse("02/10/2022");

            //double days3 = (DateTime.Now - createdDate).TotalDays;
            //if (days3 >= 92)
            //{
            //    // get current date
            //    int h = 9;
            //}

            if (!Directory.Exists(BackupFolder))
            {
                Directory.CreateDirectory(BackupFolder);
            }

            // total files
            DirectoryInfo folder = new DirectoryInfo(sourceFolder);
            FileInfo[] fileCount = folder.GetFiles(searchTxt);
            lblTotalFiles.Text = $"Total files :: {fileCount.Length}";
            // end total files


            // GetAllFiles(sourceFolder, searchTxt);
            StartAddingFiles();
        }

        private bool GetAllFiles(string sPath, string searchText, string backupFodler, ManualResetEvent eventStop, ManualResetEvent eventStopped)
        {
            bool result = false;

            m_EventStop = eventStop;
            m_EventStopped = eventStopped;

            DateTime currentDate = DateTime.Now;

            // source folder
            // \\ora-fs1.oraero.com\WebFiles\XML\Submission
            string fileName = string.Empty;
            long fileSize = 0;

            DateTime createdDate = DateTime.Now;
            DateTime lastWriteDate = DateTime.Now;
            DateTime lastAccessDate = DateTime.Now;

            Guid myGuid = Guid.NewGuid();

            int i = 1;
            int m = 0;

            // logging
            TraceSource traceSource = new TraceSource(START_APP_NAME);
            Trace.CorrelationManager.ActivityId = myGuid;
            traceSource.TraceEvent(TraceEventType.Start, 256, $"{START_APP_NAME} is starting right now.");

            if (Directory.Exists(sPath))
            {
                result = true;

                DirectoryInfo sourceFolder = new DirectoryInfo(sPath);
                FileInfo[] filesList = sourceFolder.GetFiles(searchText);

                //long total = filesList.Length;
                TotalCount = filesList.Length;
                traceSource.TraceEvent(TraceEventType.Information, 256, $"{START_APP_NAME} Looping through total files '{TotalCount}'.");
                // lblTotalFiles.Text = $"Total files :: {TotalCount}";

                FileInfoAttributes fileInfo = new FileInfoAttributes();

                foreach (FileInfo singleFile in filesList)
                {
                    fileName = singleFile.Name;
                    fileInfo.FileName = fileName;

                    createdDate = singleFile.CreationTime;
                    fileInfo.CreatedDate = createdDate;

                    lastWriteDate = singleFile.LastWriteTime;
                    fileInfo.LastWriteDate = lastWriteDate;

                    lastAccessDate = singleFile.LastAccessTime;
                    fileInfo.LastAccessDate = lastAccessDate;

                    fileSize = singleFile.Length;
                    fileInfo.Size = fileSize;

                    // create a new record
                    if (checkBoxIfAddToDb.Checked)
                    { 
                        SqlDbLayer.InsertNewRecord(fileInfo);
                    }
                  
                    // compare dates and when date is less than
                    if ((DateTime.Now - createdDate).TotalDays >= 93)
                    {
                        try
                        {
                            m++;
                            File.Move(singleFile.FullName, Path.Combine(backupFodler, singleFile.Name));
                        }
                        catch(Exception exc)
                        {
                            traceSource.TraceEvent(TraceEventType.Error, 2, $"File name: '{singleFile.Name}' Error:: {exc.Message}");
                        }

                    }

                    // add the event
                    if (m_EventStop.WaitOne(0, true))
                    {
                        // clean-up operations may be placed here
                        // inform main thread that this thread stopped
                        m_EventStopped.Set();
                        return (true);
                    }

                    this.Invoke(mDelegateDisplayFilesProcess, new object[] { fileName, i });

                    i++;
                } // end of the Loop


            } // end if directory Exists

            traceSource.TraceEvent(TraceEventType.Information, 8, $"{START_APP_NAME} Total files moved/deleted '{m}'.");
            traceSource.TraceEvent(TraceEventType.Stop, 512, $"{START_APP_NAME} is stopping.");

            traceSource.Flush();
            traceSource.Close();

            // Make asynchronous call to main form
            // to inform it that thread finished
            this.Invoke(mDelegateThreadFinished, null);

            return result;
        }

        private void WorkingAddingFiles()
        {
            bool result = false;

            try
            {
                string sourceFolder = txtSourceFolder.Text;
                string searchTxt = txtSearchFiles.Text;

                result = GetAllFiles(sourceFolder, searchTxt, BackupFolder, m_EventStopThread, m_EventThreadStopped);

                if (result)
                {
                    MessageBox.Show("Adding Files Success", "OK");
                }
            }

            catch (Exception err)
            {
                throw err;
            }

            finally
            {
            }
        }

        // Begin adding files
        private void StartAddingFiles()
        {
            // Reset Events
            m_EventStopThread.Reset();
            m_EventThreadStopped.Reset();

            // create worker thread instance
            m_WorkerThread = new Thread(new ThreadStart(this.WorkingAddingFiles));
            m_WorkerThread.Priority = ThreadPriority.Highest;

            // looks nice in Output window
            m_WorkerThread.Name = START_APP_NAME;
            m_WorkerThread.Start();
        }

        // Stop Thread
        public void StopThread()
        {
            if (m_WorkerThread != null && m_WorkerThread.IsAlive)  // thread is active
            {
                // set event "Stop"
                m_EventStopThread.Set();

                // wait when thread  will stop or finish
                while (m_WorkerThread.IsAlive)
                {
                    // We cannot use here infinite wait because our thread
                    if (WaitHandle.WaitAll((new ManualResetEvent[] { m_EventThreadStopped }), 100, true))
                    {
                        break;
                    }
                    Application.DoEvents();
                }
            }

            // set initial state of buttons
            ThreadFinished();
        }

        // Thread Finish
        public void ThreadFinished()
        {
            buttonStart.Enabled = true;
           // btnStopCoping.Enabled = false;
        }

        public void DisplayFilesProcess(string sFileName, int iCount)
        {
            txtStatus.Text = sFileName;
            txtFileCount.Text = iCount.ToString();

            int incrementProgressBarValue = (iCount * progressBarStatus.Maximum) / TotalCount;
            if (incrementProgressBarValue <= 100)
            {
                progressBarStatus.Value = incrementProgressBarValue;
                lblPercantageInfo.Text = $"% {incrementProgressBarValue}";
            }

        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            StopThread();
        }

        private void buttonCalc_Click(object sender, EventArgs e)
        {
            int myCount = 160000;

            int total = 0;

            for (int i = 1; i < myCount; i ++)
            {
                // formula used to calculate percentage is: (value/total value)×100%
                total = (i * progressBarStatus.Maximum) / myCount;
                
                if (i > 100000)
                {
                    int j = -1;
                }
                if (i == 159999)
                {
                    int l = -1;
                }
                //progressBarStatus.Value = total;
            }

            int g = 0;

            // formula used to calculate percentage is: (value/total value)×100%
        }
    }
}
