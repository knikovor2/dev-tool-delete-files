﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using FileManagmentService.Code;

namespace FileManagmentService
{
    public partial class FileManagmentService : ServiceBase
    {

        private string SourceFolder { get; set; }
        private string SearchTxt { get; set; }

        private int TotalFilesCount { get; set; }

        private string BackupFolder { get; set; }

        private int SkipTotalDays { get; set; }

        private TraceSource log = null;

        private string AppName = "FileManagmentService";

        private Guid logGuid = Guid.Empty;

        private double intervalMiliSeconds = 0;


        private Timer currentTimer = null; 


        public FileManagmentService()
        {
            InitializeComponent();

            // read variables
            SourceFolder = @ConfigurationManager.AppSettings["SourceFolder"].ToString();
            SearchTxt = ConfigurationManager.AppSettings["SearchTxt"].ToString();
            SkipTotalDays = int.Parse(ConfigurationManager.AppSettings["SkipTotalDays"].ToString());

        }

        protected override void OnStart(string[] args)
        {
            // log.TraceEvent(TraceEventType.Start, 256, $"{AppName} is starting right now.");

            log = new TraceSource(AppName);
            logGuid = Guid.NewGuid();
            Trace.CorrelationManager.ActivityId = logGuid;

            // Re-Load variables on start again
            SourceFolder = @ConfigurationManager.AppSettings["SourceFolder"].ToString();
            SearchTxt = ConfigurationManager.AppSettings["SearchTxt"].ToString();
            SkipTotalDays = int.Parse(ConfigurationManager.AppSettings["SkipTotalDays"].ToString());

            intervalMiliSeconds = double.Parse(ConfigurationManager.AppSettings["IntervalMiliSeconds"].ToString());

            log.TraceEvent(TraceEventType.Information, 8, $"{AppName} :: Service Started with time interval: '{intervalMiliSeconds}' skip days '{SkipTotalDays}'.");


            currentTimer = new Timer(intervalMiliSeconds);
            currentTimer.Interval = intervalMiliSeconds; // 600000;

            currentTimer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            
            currentTimer.Enabled = true;
            currentTimer.AutoReset = true;

            currentTimer.Start();

            log.TraceEvent(TraceEventType.Information, 8, $"{AppName} :: Timer completed.");
            log.Flush();
            log.Close();

        }

        protected override void OnStop()
        {
            log = new TraceSource(AppName);
            logGuid = Guid.NewGuid();
            Trace.CorrelationManager.ActivityId = logGuid;

            log.TraceEvent(TraceEventType.Information, 8, $"{AppName} :: Service was Stopped.");

            log.Flush();
            log.Close();
        }

        private void OnTimer(object sender, ElapsedEventArgs args)
        {
            DateTime currentDate = DateTime.Now;

            log = new TraceSource(AppName);
            logGuid = Guid.NewGuid();
            Trace.CorrelationManager.ActivityId = logGuid;

            log.TraceEvent(TraceEventType.Information, 2, $"{AppName} :: Timer started on Day: {currentDate.DayOfWeek}.");

            if (currentDate.DayOfWeek == DayOfWeek.Friday)
            {
                BeginProcessingFiles();
            }

            log.TraceEvent(TraceEventType.Information, 2, $"{AppName} :: Timer completed on Day: {currentDate.DayOfWeek}.");

            log.Flush();
            log.Close();
        }

        private void BeginProcessingFiles()
        {
            try
            {
                //log = new TraceSource(AppName);
                //logGuid = Guid.NewGuid();
                //Trace.CorrelationManager.ActivityId = logGuid;

                BackupFolder = Path.Combine(SourceFolder, $"Backup_{DateTime.Now.Year}_{DateTime.Now.Month}_{DateTime.Now.Day}");

                if (!Directory.Exists(BackupFolder))
                {
                    Directory.CreateDirectory(BackupFolder);
                }

                DateTime createdDate = DateTime.Now;
                DateTime lastWriteDate = DateTime.Now;
                DateTime lastAccessDate = DateTime.Now;

                string fileName = string.Empty;
                
                long fileSize = 0;
                int i = 0;
                int m = 0;

                if (Directory.Exists(SourceFolder))
                {
                    // result = true;

                    DirectoryInfo sourceFolder = new DirectoryInfo(SourceFolder);
                    FileInfo[] filesList = sourceFolder.GetFiles(SearchTxt);

                    TotalFilesCount = filesList.Length;
                    log.TraceEvent(TraceEventType.Information, 256, $"{AppName} Looping through total files '{TotalFilesCount}'.");

                    FileInfoAttributes fileInfo = new FileInfoAttributes();

                    foreach (FileInfo singleFile in filesList)
                    {
                        fileName = singleFile.Name;
                        fileInfo.FileName = fileName;

                        createdDate = singleFile.CreationTime;
                        fileInfo.CreatedDate = createdDate;

                        lastWriteDate = singleFile.LastWriteTime;
                        fileInfo.LastWriteDate = lastWriteDate;

                        lastAccessDate = singleFile.LastAccessTime;
                        fileInfo.LastAccessDate = lastAccessDate;

                        fileSize = singleFile.Length;
                        fileInfo.Size = fileSize;

                        // create a new record
                        //  SqlDbLayer.InsertNewRecord(fileInfo);
                        
                        // compare dates and when date is less than 92 reading from config file
                        if ((DateTime.Now - createdDate).TotalDays >= SkipTotalDays)
                        {
                            try
                            {
                                m++;
                                File.Move(singleFile.FullName, Path.Combine(BackupFolder, singleFile.Name));
                            }
                            catch (Exception exc)
                            {
                                log.TraceEvent(TraceEventType.Error, 2, $"File name: '{singleFile.Name}' Error:: {exc.Message}");
                            }

                        }

                        i++;
                    } // end of the Loop


                } // end if directory Exists

                log.TraceEvent(TraceEventType.Information, 8, $"{AppName} Total files moved/deleted '{m}'.");

               // log.Flush();
              //  log.Close();
            }
            catch (Exception ex)
            {
                log.TraceEvent(TraceEventType.Error, 2, $"{AppName} :: Error: {ex.ToString()} {ex.StackTrace}.");

               // log.Flush();
               // log.Close();
            }
        }
    }
}
