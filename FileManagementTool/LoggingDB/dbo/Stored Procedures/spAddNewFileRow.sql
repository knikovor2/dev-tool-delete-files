﻿/*===================================================================================
File name:		spAddNewFileRow.sql
Author:			Krassimir Nikov
Create Date:	2022-05-18
Description:	add a record for new file in database
				

--------------------------------------------------------------------------------------
Summary of Changes
Date:			Author			Comments            CARD
--------------------------------------------------------------------------------------
2022-05-18      KNikov          Initial creation    ADMIN-504

===================================================================================*/

CREATE PROCEDURE dbo.spAddNewFileRow
(
       @FILE_NAME AS varchar(255),
       @CREATE_DATE AS datetime,
       @LAST_ACCESS_DATE as datetime,
       @LAST_WRITE_DATE as datetime,
       @FILE_SIZE as BIGINT
)
AS

BEGIN

       INSERT INTO dbo.xmlFiles
       ( 
			-- [FileId], 
			[FileName], 
			FileSize, 
			CreateDate, 
			LastWriteDate, 
			LastAccessDate, 
			RowAddedDate
       ) 
       VALUES 
       (
              @FILE_NAME, 
              @FILE_SIZE, 
              @CREATE_DATE, 
              @LAST_WRITE_DATE, 
              @LAST_ACCESS_DATE, 
              GETDATE()
       )

END
