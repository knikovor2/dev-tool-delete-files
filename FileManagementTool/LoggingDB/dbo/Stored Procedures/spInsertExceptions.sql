﻿/*===================================================================================
File name:		spInsertExceptions.sql
Author:			Krassimir Nikov
Create Date:	2022-05-18
Description:	add new record for Exceptions or info
				

--------------------------------------------------------------------------------------
Summary of Changes
Date:			Author			Comments            CARD
--------------------------------------------------------------------------------------
2022-05-18      KNikov          Initial creation    ADMIN-504

===================================================================================*/

CREATE PROCEDURE dbo.spInsertExceptions
(
       @ACTIVITI_ID AS UNIQUEIDENTIFIER,
       @EVENT_TYPE AS VARCHAR(50),
       @MESSAGE as VARCHAR(8000),
       @THREAD_ID as VARCHAR(50),
       @PROCESS_ID as INT,
       @SOURCE as VARCHAR(50)
)
AS

BEGIN
       INSERT INTO dbo.Exceptions
       ( 
			ActivityId, 
			EventType, 
			[Message], 
			ThreadId, 
			ProcessId, 
			[Source], 
			CreateDate
       ) 
       VALUES 
       (
              @ACTIVITI_ID, 
              @EVENT_TYPE, 
              @MESSAGE, 
              @THREAD_ID, 
              @PROCESS_ID, 
              @SOURCE, 
              GETDATE()
       )
END
