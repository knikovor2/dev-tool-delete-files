﻿CREATE TABLE dbo.Exceptions (
    LogId      BIGINT           IDENTITY (1, 1) NOT NULL,
    ActivityId UNIQUEIDENTIFIER NULL,
    EventType  VARCHAR (150)    NULL,
    [Message]    VARCHAR (8000)   NULL,
    ThreadId   VARCHAR (150)    NULL,
    ProcessId  INT              NULL,
    [Source]     VARCHAR (150)    NULL,
    CreateDate DATETIME         NULL,
    CONSTRAINT PK_Exceptions_LogId PRIMARY KEY CLUSTERED (LogId ASC)
);

