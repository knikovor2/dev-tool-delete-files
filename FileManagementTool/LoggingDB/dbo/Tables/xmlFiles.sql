﻿CREATE TABLE [dbo].[xmlFiles] (
    [FileId]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [FileName]       VARCHAR (255) NULL,
    [FileSize]       BIGINT        NULL,
    [CreateDate]     DATETIME      NULL,
    [LastWriteDate]  DATETIME      NULL,
    [LastAccessDate] DATETIME      NULL,
    [RowAddedDate]   DATETIME      NULL,
    CONSTRAINT [PK_xmlFiles_FileId] PRIMARY KEY CLUSTERED ([FileId] ASC)
);

